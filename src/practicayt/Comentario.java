package practicayt;

class Comentario {
    private String texto;
    private String usuario;
    private String fecha;

    public Comentario(String texto, String usuario, String fecha) {
        this.texto = texto;
        this.usuario = usuario;
        this.fecha = fecha;
    }

   
    public String toString() {
        return "Comentario: \"" + texto + "\" del usuario: " + usuario + " en fecha: " + fecha;
    }
}

