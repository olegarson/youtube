package practicayt;
import java.util.ArrayList;
import java.util.Scanner;

class Comentario1 {
    private String texto;
    private String usuario;
    private String fecha;

    public Comentario1(String texto, String usuario, String fecha) {
        this.texto = texto;
        this.usuario = usuario;
        this.fecha = fecha;
    }

	@Override
    public String toString() {
        return "Comentario: \"" + texto + "\" del usuario: " + usuario + " en fecha: " + fecha;
    }
}

class Video {
    private String titulo;
    private int likes;
    private String fecha;
    private ArrayList<Comentario1> comentarios;

    public Video(String titulo, String fecha) {
        this.titulo = titulo;
        this.likes = 0;
        this.fecha = fecha;
        this.comentarios = new ArrayList<>();
    }

    public String getTitulo() {
        return titulo;
    }

    public int getLikes() {
        return likes;
    }

    public void darLike() {
        likes++;
        String red="\033[31m"; 
		System.out.println(red +"ME GUSTA");
    }

    public String getFecha() {
        return fecha;
    }

    public ArrayList<Comentario1> getComentarios() {
        return comentarios;
    }

    public void agregarComentario(String texto, String usuario, String fecha) {
        Comentario1 nuevoComentario = new Comentario1(texto, usuario, fecha);
        comentarios.add(nuevoComentario);
        System.out.println("Nuevo comentario añadido: " + nuevoComentario);
    }

    public void mostrarComentarios() {
        if (!comentarios.isEmpty()) {
            System.out.println("Comentarios del video:");
            for (Comentario1 comentario : comentarios) {
                System.out.println(comentario);
            }
        } else {
            System.out.println("No hay comentarios en este video.");
        }
    }

    public void mostrarInfoVideo() {
        System.out.println("Información del video:");
        System.out.println("Video: \"" + titulo + "\" en fecha: " + fecha + " con " + likes + " likes y " + comentarios.size() + " comentarios");
    }

    public void mostrarMenu() {
        System.out.println("| -- " + titulo + " -- |");
        System.out.println("1- Nuevo comentario");
        System.out.println("2- Like");
        System.out.println("3- Mostrar Comentarios");
        System.out.println("4- Mostrar estadísticas");
        System.out.println("0- Salir");
    }

    public void ejecutarMenu() {
        Scanner sc = new Scanner(System.in);
        int opcion;

        do {
            mostrarMenu();
            System.out.print("Selecciona una opción: ");
            opcion = sc.nextInt();

            switch (opcion) {
                case 1:
                    nuevoComentario();
                    break;
                case 2:
                    darLike();
                    break;
                case 3:
                    mostrarComentarios();
                    break;
                case 4:
                    mostrarInfoVideo();
                    break;
                case 0:
                    System.out.println("Saliendo del video: " + titulo);
                    break;
                default:
                    System.out.println("Opción no válida. Inténtalo de nuevo.");
                    break;
            }

        } while (opcion != 0);
    }

    private void nuevoComentario() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce comentario y nombre de usuario (Separados por espacio): ");
        String[] entrada = sc.nextLine().split(" ");
        if (entrada.length >= 2) {
            String textoComentario = entrada[0];
            String nombreUsuario = entrada[1];
            String fechaComentario = "21-03-2019";  //
            agregarComentario(textoComentario, nombreUsuario, fechaComentario);
        } else {
            System.out.println("Entrada no válida. Inténtalo de nuevo.");
        }
    }

    private void darLike1() {
        likes++;
        String red="\033[31m"; 
		System.out.println(red +"ME GUSTA");
 // 
    }
}

