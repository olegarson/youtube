package practicayt;
import java.util.Scanner;
import java.util.ArrayList;

class Canal {
    private String nombre;
    private String fechaCreacion;
    private ArrayList<Video> videos;
    private Video videoSeleccionado;

    public Canal(String nombre, String fechaCreacion) {
        this.nombre = nombre;
        this.fechaCreacion = fechaCreacion;
        this.videos = new ArrayList<>();
        this.videoSeleccionado = null;
    }

    public String getNombre() {
        return nombre;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public ArrayList<Video> getVideos() {
        return videos;
    }

    public Video getVideoSeleccionado() {
        return videoSeleccionado;
    }

    public void setVideoSeleccionado(Video videoSeleccionado) {
        this.videoSeleccionado = videoSeleccionado;
    }

    public void agregarVideo(Video video) {
        videos.add(video);
        videoSeleccionado = video;
    }

    public int getTotalVideos() {
        return videos.size();
    }

    public void mostrarMenu() {
        System.out.println("|————————|");
        System.out.println("— " + nombre + " —");
        System.out.println("1- Nuevo video");
        System.out.println("2- Seleccionar vídeo");
        System.out.println("3- Mostrar estadísticas");
        System.out.println("4- Mostrar info videos");
        System.out.println("0- Salir");
    }

    public void ejecutarMenu() {
        Scanner sc = new Scanner(System.in);
        int opcion;

        do {
            mostrarMenu();
            System.out.print("Selecciona una opción: ");
            opcion = sc.nextInt();

            switch (opcion) {
                case 1:
                    nuevoVideo();
                    break;
                case 2:
                    seleccionarVideo();
                    break;
                case 3:
                    mostrarEstadisticas();
                    break;
                case 4:
                    mostrarInfoVideos();
                    break;
                case 0:
                    System.out.println("Saliendo del canal: " + nombre);
                    break;
                default:
                    System.out.println("Opción no válida. Inténtalo de nuevo.");
                    break;
            }

        } while (opcion != 0);
    }

    private void nuevoVideo() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el nombre del nuevo video: ");
        String nombreVideo = sc.nextLine();
        Video nuevoVideo = new Video(nombreVideo, nombreVideo);
        agregarVideo(nuevoVideo);
        System.out.println("Nuevo video creado: " + nombreVideo);
    }

    private void seleccionarVideo() {
        if (videos.isEmpty()) {
            System.out.println("No hay videos disponibles. Creando uno automáticamente.");
            nuevoVideo();
        } else {
            System.out.println("Selecciona un video:");
            for (int i = 0; i < videos.size(); i++) {
                System.out.println(i + "- Video: \"" + videos.get(i).getTitulo() + "\" en fecha: " + videos.get(i).getFecha() + " con " + videos.get(i).getLikes() + " likes y " + videos.get(i).getComentarios() + " comentarios");
            }
            Scanner sc = new Scanner(System.in);
            int seleccion = sc.nextInt();
            if (seleccion >= 0 && seleccion < videos.size()) {
                videoSeleccionado = videos.get(seleccion);
                System.out.println("Video seleccionado: " + videoSeleccionado.getTitulo());
            } else {
                System.out.println("Selección no válida. Seleccionando automáticamente el primer video.");
                videoSeleccionado = videos.get(0);
            }
        }
    }

    private void mostrarEstadisticas() {
        if (videoSeleccionado != null) {
            System.out.println("Estadísticas del video seleccionado: \"" + videoSeleccionado.getTitulo() + "\" en fecha: " + videoSeleccionado.getFecha() + " con " + videoSeleccionado.getLikes() + " likes y " + videoSeleccionado.getComentarios() + " comentarios");
        } else {
            System.out.println("No hay video seleccionado. Por favor, selecciona un video primero.");
        }
    }

    private void mostrarInfoVideos() {
        if (!videos.isEmpty()) {
            System.out.println("Información de todos los videos:");
            for (Video video : videos) {
                System.out.println(video);
            }
        } else {
            System.out.println("No hay videos disponibles en este canal.");
        }
    }

	public String getTotalComentarios() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getTotalLikes() {
		// TODO Auto-generated method stub
		return null;
	}
}



