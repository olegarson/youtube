package practicayt;
import java.util.ArrayList;
import java.util.Scanner;

class videoyt {
    private String titulo;
    private String fecha;
    private int likes;
    private int comentarios;

    public videoyt(String titulo, String fecha) {
        this.titulo = titulo;
        this.fecha = fecha;
        this.likes = 0;
        this.comentarios = 0;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getFecha() {
        return fecha;
    }

    public int getLikes() {
        return likes;
    }

    public void darLike() {
        likes++;
    }

    public int getComentarios() {
        return comentarios;
    }

    public void agregarComentario() {
        comentarios++;
    }
}

class canal {
    private String nombre;
    private String fechaCreacion;
    private ArrayList<Video> videos;

    public canal(String nombre, String fechaCreacion) {
        this.nombre = nombre;
        this.fechaCreacion = fechaCreacion;
        this.videos = new ArrayList<>();
    }

	public String getNombre() {
        return nombre;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public ArrayList<Video> getVideos() {
        return videos;
    }

    public void agregarVideo(Video video) {
        videos.add(video);
    }

    public int getTotalLikes() {
        int totalLikes = 0;
        for (Video video : videos) {
            totalLikes += video.getLikes();
        }
        return totalLikes;
    }

    public ArrayList<Comentario1> getTotalComentarios() {
        ArrayList<Comentario1> totalComentarios = null;
        for (Video video : videos) {
            totalComentarios = video.getComentarios();
        }
        return totalComentarios;
    }
}

public class Youtube {
    private static ArrayList<Canal> canales = new ArrayList<>();
    private static Canal canalSeleccionado = null;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int opcion;

        do {
            mostrarMenu();
            System.out.print("Selecciona una opción: ");
            opcion = sc.nextInt();

            switch (opcion) {
                case 0:
                    System.out.println("Has salido del programa.");
                    break;
                case 1:
                    nuevoCanal();
                    break;
                case 2:
                    seleccionarCanal();
                    break;
                case 3:
                    mostrarEstadisticas();
                    break;
                case 4:
                    mostrarEstadisticasCompletas();
                    break;
                default:
                    System.out.println("Opción no válida. Inténtalo de nuevo.");
                    break;
            }

        } while (opcion != 0);
    }

    private static void mostrarMenu() {
        System.out.println("--- YOUTUBE ---");
        System.out.println("1 - Nuevo canal");
        System.out.println("2 - Seleccionar canal");
        System.out.println("3 - Mostrar estadísticas");
        System.out.println("4 - Mostrar estadísticas completas");
        System.out.println("0 - Salir");
        System.out.println("|-----------------------|");
    }

    private static void nuevoCanal() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el nombre del canal: ");
        String nombreCanal = sc.nextLine();
        String fechaCreacion = "21-03-2019";  
        Canal nuevoCanal = new Canal(nombreCanal, fechaCreacion);
        canales.add(nuevoCanal);
        canalSeleccionado = nuevoCanal;
        System.out.println("Canal creado y seleccionado: " + nombreCanal);
    }

    private static void seleccionarCanal() {
        if (canales.isEmpty()) {
            System.out.println("No hay canales disponibles. Creando uno automáticamente.");
            nuevoCanal();
        } else {
            System.out.println("Selecciona un canal:");
            for (int i = 0; i < canales.size(); i++) {
                System.out.println(i + "- Nombre del canal: \"" + canales.get(i).getNombre() + "\" creado en fecha: " + canales.get(i).getFechaCreacion() + " con " + canales.get(i).getVideos().size() + " videos");
            }
            Scanner sc = new Scanner(System.in);
            int seleccion = sc.nextInt();
            if (seleccion >= 0 && seleccion < canales.size()) {
                canalSeleccionado = canales.get(seleccion);
                System.out.println("Canal seleccionado: " + canalSeleccionado.getNombre());
            } else {
                System.out.println("Selección no válida. Seleccionando automáticamente el primer canal.");
                canalSeleccionado = canales.get(0);
            }
        }
    }

    private static void mostrarEstadisticas() {
        if (canalSeleccionado != null) {
            System.out.println("Estadísticas del canal: \"" + canalSeleccionado.getNombre() +
                    "\" creado en fecha: " + canalSeleccionado.getFechaCreacion() +
                    " con " + canalSeleccionado.getVideos().size() + " videos");
            System.out.println("Total de likes: " + canalSeleccionado.getTotalLikes());
            System.out.println("Total de comentarios: " + canalSeleccionado.getTotalComentarios());
        } else {
            System.out.println("No hay canal seleccionado. Por favor, selecciona un canal primero.");
        }
    }

    private static void mostrarEstadisticasCompletas() {
        System.out.println("Estadísticas completas:");
        for (Canal canal : canales) {
            System.out.println("- Nombre del canal: \"" + canal.getNombre() + "\" creado en fecha: " + canal.getFechaCreacion() + " con " + canal.getVideos().size() + " videos");
            for (Video video : canal.getVideos()) {
                System.out.println("-- Video: \"" + video.getTitulo() + "\" en fecha: " + video.getFecha() + " con " + video.getLikes() + " likes y " + video.getComentarios() + " comentarios");
            }
        }
    }
}
